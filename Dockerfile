FROM ubuntu:latest

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=develop
ENV branch=$branch

RUN apt-get update && \
    apt-get install -y unzip \
    cmake \
    make \
    bsdmainutils \
    curl \
    xxd \
    autotools-dev \
    autoconf \
    automake \
    pkg-config \
    perl \
    git patch \
    gcc g++ \
    libtool \
    sudo \
    wget python3 && \
    useradd -d /developer -m developer && \
    usermod -aG sudo developer && \
    chown -R developer:developer /developer && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

ENV HOME=/developer
ENV VITASDK=/opt/vitasdk
ENV PATH="${VITASDK}/bin:${PATH}"

RUN git clone https://github.com/vitasdk/vdpm.git --depth=1 && \
    cd vdpm/ && chmod +x ./*.sh && \
    ./bootstrap-vitasdk.sh && \
    ./install-all.sh && cd .. && rm -fr vdpm/

USER developer
WORKDIR /developer
VOLUME /developer

CMD make
